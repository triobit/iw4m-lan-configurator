﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Triobit_Configurator {
    public partial class frmMain : Form {
        private string ConfigFile = Path.Combine(Application.StartupPath, "iw4m-lan.ini");
        private ConfigParser Config = null;

        public frmMain() {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e) {
            AcceptButton = cmdSave;

            if (!loadConfig()) {
                MessageBox.Show("Failed to load your username and Steam-ID.");
            } else {
                saveConfig(); // Save again, to make sure everything is in sync
            }
        }

        private void cmdSave_Click(object sender, EventArgs e) {
            if (saveConfig()) {
                MessageBox.Show("Your info has been saved", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else {
                MessageBox.Show("Failed to save your information. Is another application accessing \"iw4m-lan.ini\"?\r\nFor support visit: www.triobit.net");
            }
        }

        private void txtSteamID_TextChanged(object sender, EventArgs e) {
            cmdSave.Enabled = txtUsername.TextLength >= 1 && txtSteamID.TextLength >= 4;

            TextBox txtSteam = (TextBox)sender;
            txtSteam.Text = validateInput(txtSteam.Text);
            txtSteam.SelectionStart = txtSteam.Text.Length;
        }

        private void txtUsername_TextChanged(object sender, EventArgs e) {
            cmdSave.Enabled = txtUsername.TextLength >= 1 && txtSteamID.TextLength >= 4;
        }

        private void lblTriobit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start("http://www.triobit.net");
        }

        private bool loadConfig() {
            try {
                if (!File.Exists(ConfigFile)) {
                    createConfig();
                } else if (Config == null) {
                    Config = new ConfigParser(ConfigFile);
                }

                txtUsername.Text = Config.GetValue("Configuration", "Nickname", "Unknown Player");
                string steamid = validateInput(Config.GetValue("Configuration", "SteamID", new Random().Next(0, 999999999).ToString()));
                if (steamid.Length > 32) {
                    steamid = steamid.Substring(0, 32);
                }

                txtSteamID.Text = steamid;

                return true;
            } catch (Exception ex) {
                MessageBox.Show("loadConfig(): " + ex.Message + "\r\n" + ex.StackTrace);
                return false;
            }
        }

        private bool saveConfig() {
            try {
                if (Config == null) {
                    Config = new ConfigParser(ConfigFile);
                }

                if (txtUsername.TextLength > 0) {
                    Config.SetValue("Configuration", "Nickname", txtUsername.Text);
                }

                if (txtSteamID.TextLength > 0) {
                    Config.SetValue("Configuration", "SteamID", txtSteamID.Text);
                }

                Config.Flush();

                return true;
            } catch (Exception ex) {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
                return false;
            }
        }

        private void createConfig() {
            if (!File.Exists(ConfigFile)) {
                File.Create(ConfigFile).Dispose();

            }

            if (Config == null) {
                Config = new ConfigParser(ConfigFile);
            }

            Config.SetValue("Configuration", "Nickname", "Unknown Player");
            Config.SetValue("Configuration", "SteamID", new Random().Next(0, 999999999));
            Config.Flush();
        }

        private string validateInput(string input) {
            string output = string.Empty;

            for (int i = 0; i < input.Length; i++) {
                if (Regex.IsMatch(input[i].ToString(), "^[0-9-]+$")) {
                    output += input[i];
                }
            }

            return output;
        }
    }
}
